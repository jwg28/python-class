# Intro to APIs

#### With Javascript :)

---

## Why are we here?

* Learn about JSON
* Learn how to make an API request in JavaScript
* Start using APIs on a page of our own!

---

## JSON

* JavaScript Object Notation
* A format for sharing data
* NOT JavaScript exclusive
* Key-Value datastore, typically rendered in curly braces

----

### JSON value data types

* strings
* numbers
* objects
* arrays
* Booleans (true or false)
* null

----

### JSON Example

```javascript
{ 
  "sammy" : {
    "username"  : "SammyShark",
    "location"  : "Indian Ocean",
    "online"    : true,
    "followers" : 987
  },
  "jesse" : {
    "username"  : "JesseOctopus",
    "location"  : "Pacific Ocean",
    "online"    : false,
    "followers" : 432
  },
  "drew" : {
    "username"  : "DrewSquid",
    "location"  : "Atlantic Ocean",
    "online"    : false,
    "followers" : 321
  }
}
```

----

### Nested JSON Example

```javascript
{ 
  "first_name" : "Sammy",
  "last_name" : "Shark",
  "location" : "Ocean",
  "websites" : [ 
    {
      "description" : "work",
      "URL" : "https://www.digitalocean.com/"
    },
    {
      "desciption" : "tutorials",
      "URL" : "https://www.digitalocean.com/community/tutorials"
    }
  ],
  "social_media" : [
    {
      "description" : "twitter",
      "link" : "https://twitter.com/digitalocean"
    },
    {
      "description" : "facebook",
      "link" : "https://www.facebook.com/DigitalOceanCloudHosting"
    }
  ]
}
```

----

### Notes on JSON

* You won't be writing pure JSON
* Most likely converting other data sources into JSON
* Can validate JSON with JSONLint
* Can test JSON in web dev context w/ JSFiddle

---

## APIs

* Application Programming Interfaces
* How we can get disparate systems to 'talk' to each other

----

### What are APIs used for?

* Fetching data from 3rd-party sources
* Used by application developers to create apps
* Can be used for services like IFTTT and Zapier

----

### How do we make an API request in JavaScript?

```javascript
function reqListener () {
  console.log(this.responseText);
}

var oReq = new XMLHttpRequest();
oReq.addEventListener("load", reqListener);
oReq.open("GET", "http://www.example.org/example.txt");
oReq.send();
```

---

## Great!

#### Now let's build our own stuff :)





    