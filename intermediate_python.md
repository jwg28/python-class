# We All Make Mistakes!

### Also, Python :)

---

## Why are we here?

### Learn some more in-depth concepts of programming

----

### Objectives for today:

* Functions
* Conditionals
* Tracing/Debugging

---

## Quick review of concepts from previous class:

----

### Programming isn't about languages

* The language you choose ultimately doesn't matter that much
* The difficulty in learning programming isn't the languages, but PARADIGMS

----

###  Contrary to popular belief, programming doesn't require a lot of memorizing

* You wouldn't memorize a dictionary, or Wikipedia right?
* Rather, you'll be learning that these concepts exist, and then utilize these manuals as references

----

### Most programming isn't about Math!

* It's mostly about logic
* The math you'll need is the math required for the problem you're trying to solve anyway

----

### Programming languages are simpler than human languages

* How many defined words are there in the English language?
* How about Python?

----

### They have painfully consistent syntax

* Programming languages don't have much, if any room, for flexibility/interpretation
* Think of the phrase 'Tell me about it!'

----

### At its core, programming is all about solving problems

----

### Programming has a few basic instructions that are present in most languages

* They work in nearly the same way in every programming language
* What we want to do is learn how to use these concepts in English

----

### What are those instructions?

* Input - get data
* Output - display data
* Math - basic mathematical operations
* Conditional Execution - check for conditions and execute
* Repetition - perform actions repeatedly

----

###  Last but not least - Comments are Code!

* One of the great misconceptions is that comments explain code to other programmers
* It's actually the other way around - Code is there to explain comments to the computer

---
  
### Concept Review

----

### New variable

* Create a variable called (Name) of type (Type) with the value (InitVal)

```python
name = initVal
```

----

### Output

* Output the text (Message)

```python
print ('message')
```

----

### Input

* Ask the user (Message) and store the answer in (Variable)

```python
variable = input('message')
```

----

### Convert to Integer

* Convert (oldVariable) to integer and store in (intVariable)

```python
intVariable = int(oldVariable)
```

----

### For Loop

* Begin with (sentry) at (start) and add (change) to sentry on each pass until sentry is larger than or equal to (finish)

```python
for sentry in range(start, finish, change):
    <code>
```

----

### While Loop

* Initialize (sentry) with (initializationCode) then continue loop as long as (condition) is true.  Inside loop, change sentry with (changeCode)

```python
initializationCode
while (condition):
    changeCode
```

---

## OK, review time over!

---

### Functions

* Named sequence of statements that belong together
* Help us organize programs into chunks that match how we think about the problem
* Define function (name) which will be passed zero or more (parameters), then perform (statements) using the given (paramenters) as input

```python
def name( parameters ):
    statements
```

----

### Function Example

```python
 def draw_square(t, sz):
     """Make turtle t draw a square of sz."""
     for i in range(4):
         t.forward(sz)
         t.left(90)

 wn = turtle.Screen()        # Set up the window
 wn.bgcolor("lightgreen")
 wn.title("Alex meets a function")

 alex = turtle.Turtle()      # Create alex
 draw_square(alex, 50)       # Call the draw function
 wn.mainloop()
```

----

### Functions can call other functions

----

### Draw a rectangle

```python
def draw_rectangle(trt, w, h):
    """Get turtle t to draw a rectangle of width w and height h."""
    for i in range(2):
        trt.forward(w)
        trt.left(90)
        trt.forward(h)
        trt.left(90)
```

----

### Patterns

* Note how in our example, we looked for a pattern
* So we drew 2 halves, instead of a whole
* Now, we can use that same pattern matching skill to draw a square!

----

### Draw a square

```python
def draw_square(trt, sz):        # A new version of draw_square
    draw_rectangle(trt, sz, sz)
```

----

### Flow of Execution

* Execution always begins at the first line of a program
* Function definitions do not alter flow of execution
* Function calls are like a detour in the flow of execution

----

### Flow of Execution Example

```python
 def draw_square(t, sz):
     """Make turtle t draw a square of sz."""
     for i in range(4):
         t.forward(sz)
         t.left(90)

 wn = turtle.Screen()        # Set up the window
 wn.bgcolor("lightgreen")
 wn.title("Alex meets a function")

 alex = turtle.Turtle()      # Create alex
 draw_square(alex, 50)       # Call the draw function
 wn.mainloop()
```

----

### Functions that require arguments

* Most functions require arguments - this provides generalization
* Functions can take more than one argument

----

### Built-in function examples

```python
>>> abs(-5)
5

>>> pow(2, 3)
8
```

----

### Functions that return values

* Functions that return values are fruitful functions
* Opposite of fruitful is void functions
* abs(), pow() are fruitful
* draw_square is void
* use `return` to return values for a fruitful function

----

### Variables and parameters are local

* We can create variables locally in a function
* These variables only exist inside the function

----

### Variable scope example

```python
def final_amt(p, r, n, t):
    a = p * (1 + r/n) ** (n*t)
    return a

print(a)
```

---

### Conditionals

* We can test conditions
* Program behavior can change based on test outcomes

----

### Boolean values and expressions

* Boolean values are `True` and `False`
* Note the capitalization

----

### Boolean expressions

```python
x == y  # Produce True if ... x is equal to y
x != y  # ... x is not equal to y
x > y   # ... x is greater than y
x < y   # ... x is less than y
x >= y  # ... x is greater than or equal to y
x <= y  # ... x is less than or equal to y
```

----

### Logical operators

```python
and     # Produce True if all conditions are true
or      # Produce True if one condition is true
not     # Negates a Boolean value
```

----

### Conditional Execution

* Check conditions
* Change behavior of program accordingly

----

### Conditional Execution Example

```python
if x % 2 == 0:
    print(x, " is even.")
else:
    print(x, " is odd.")
```

----

### Conditional Execution Options

* Omitting the else clause
* Chained Conditionals
* Nested Conditionals

---

### Debugging

----

### What is code tracing/debugging?

* A method for simulating code execution to verify that programs work as expected
* Used to diagnose and solve issues that are present in code that is already live

----

### Debugging Example

```python
 def draw_square(t, sz):
     """Make turtle t draw a square of sz."""
     for i in range(4):
        print('Loop iteration ',i)
        print('Size is ',sz)
        t.forward(sz)
        t.left(90)

 wn = turtle.Screen()        # Set up the window
 wn.bgcolor("lightgreen")
 wn.title("Alex meets a function")

 alex = turtle.Turtle()      # Create alex
 draw_square(alex, 50)       # Call the draw function
 wn.mainloop()
```

----

### More Debugging Examples (with turtles!)

---

### What did we learn today?

----

* Overview of basic concepts
* Functions
* Conditionals
* Debugging

----

## Thank You!

If you're interested in more classes, visit colab.duke.edu/roots
    