# Intro to Machine Learning

### With Python :)

---

## Why are we here?

### Get a basic introduction to Machine Learning
### Complete a small machine learning project using Python

---

## What will we be doing today?

* Introduce Python SciPy, a useful package for machine learning in Python
* Load a dataset 
* Understand the dataset's structure using statistical summaries and data visualization
* Create 6 machine learning models
* Choose the best learning model, and build confidence that the accuracy is reliable

----

## How Do You Start Machine Learning in Python?

* The best way to learn machine learning is by designing and completing small projects.

----

## Python Can Be Intimidating When Getting Started

* Python is a popular and powerful interpreted language
* Python is a complete language and platform
* Python can be used for both research and development, as well as developing production systems
* There are also a lot of modules and libraries to choose from

----

## The best way to get started using Python for machine learning?

### Complete a project!

----

## Completing a project

* Introduces you to the Python interpreter (at the very least)
* Gives you a bird’s eye view of how to step through a small project
* Provides confidence, so that you can go on to your own small projects

----

### When you are applying machine learning to your own datasets, you are working on a project!

----

## A machine learning project may not be linear
#### It does has a number of well known steps though:

* Define Problem
* Prepare Data
* Evaluate Algorithms
* Improve Results
* Present Results

----

## These steps are our template for machine learning

* We can use this on dataset after dataset
* We can fill in data preparation/improving results later
* It's all about repetition, and confidence!

---

## Hello World of Machine Learning

----

### What's the best small project to work on?

* Classification of iris flowers (e.g. the iris dataset).

----

### Why is this a good project?

* Attributes are numeric so you have to figure out how to load and handle data
* It is a classification problem, allowing you to practice with an easier type of supervised learning algorithm
* It only has 4 attributes and 150 rows, meaning it is small
* All of the numeric attributes are in the same units and the same scale
* Doesn't require any special scaling or transforms to get started

----

### Let’s get started with our first project!

---

### What will we be covering?

* Introduce the Python and SciPy platform
* Loading the dataset
* Summarizing the dataset
* Visualizing the dataset
* Evaluating some algorithms
* Making some predictions

----

### Using Python SciPy

----

#### There are 5 key libraries that we will be using:

* scipy
* numpy
* matplotlib
* pandas
* sklearn

----

### Start Python and check versions

```python
# Check the versions of libraries
# Python version
import sys
print('Python: {}'.format(sys.version))
# scipy
import scipy
print('scipy: {}'.format(scipy.__version__))
# numpy
import numpy
print('numpy: {}'.format(numpy.__version__))
# matplotlib
import matplotlib
print('matplotlib: {}'.format(matplotlib.__version__))
# pandas
import pandas
print('pandas: {}'.format(pandas.__version__))
# scikit-learn
import sklearn
print('sklearn: {}'.format(sklearn.__version__))
```

----

### Load The Data

* We are going to use the iris flowers dataset - the “hello world” for machine learning
* This dataset contains 150 observations of iris flowers. 
* There are four columns of measurements of the flowers in centimeters
* The fifth column is the species of the flower observed
* All observed flowers belong to one of three species

----

### Load libraries

```python
import pandas
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
```

----

### Load dataset

```python
filename = "iris.csv"
# filename = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/iris.csv"
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = pandas.read_csv(filename, names=names)
```

----

### Analyze the data in a few ways:

* Dimensions of the dataset
* Peek at the data itself
* Statistical summary of all attributes
* Breakdown of the data by the class variable

----

### Dimensions of Dataset

----

#### We can get a quick idea of how many instances (rows) and how many attributes (columns) in the data:
	
```python
# shape
print(dataset.shape)
```

----

### Peek at the Data

----

### It is also always a good idea to actually eyeball your data.

```python
# head
print(dataset.head(20))
```

----

### Statistical Summary

----

### High-level overview of count, mean, the min and max values, as well as some percentiles

```python
# descriptions
print(dataset.describe())
```

----

### Class Distribution

----

### View the number of instances (rows) that belong to each class
	
```python
# class distribution
print(dataset.groupby('class').size())
```

---

## Data Visualization

----

### We now have a basic idea about the data
#### Let's extend that with some visualizations.

----

Two types of plots:

* Univariate plots to better understand each attribute.
* Multivariate plots to better understand the relationships between attributes.

----

### Univariate Plots

* Plots of each individual variable
* Since input variables are numeric, we can create box and whisker plots of each

----

#### Box and Whisker Plots

```python
# box and whisker plots
dataset.plot(kind='box', subplots=True, layout=(2,2), sharex=False, sharey=False)
plt.show()
# the following is just for our online python interpreter
plt.savefig('graph.png')
```

----

### Histograms are a way for us to get an idea of data distribution

----

#### Histograms

```python
# histograms
dataset.hist()
plt.show()
# the following is just for our online python interpreter
plt.savefig('graph.png')
```

----

## Multivariate Plots

* Now we can look at the interactions between the variables
* First, let’s look at scatterplots of all pairs of attributes
* Scatterplots help us spot structured relationships between input variables

----

### Scatter Plot

```python
# scatter plot matrix
scatter_matrix(dataset)
plt.show()
# the following is just for our online python interpreter
plt.savefig('graph.png')
```

----

### Observations

* Note the diagonal grouping of some pairs of attributes
* This suggests a high correlation and a predictable relationship

---

## Evaluate Some Algorithms!

### Time to create some models of the data and estimate their accuracy on unseen data

----

### What we'll be doing:

* Separate out a validation dataset
* Set-up the test harness to use 10-fold cross validation
* Build 5 different models to predict species from flower measurements
* Select the best model

----

### Create a Validation Dataset

* We need to know if the model we created is any good
* We will use statistical methods to estimate the accuracy of the models that we create on unseen data
* We want a more concrete estimate of the accuracy of the best model on unseen data

----

### How do we do that?

* We hold back some data that the algorithms will not get to see
* We will use this data to get a second and independent idea of how accurate the best model might actually be
* Split the loaded dataset into two
* 80% will be our training set
* 20% will be our validation set

----

### Validation Dataset

```python
# Split-out validation dataset
array = dataset.values
X = array[:,0:4]
Y = array[:,4]
validation_size = 0.20
seed = 7
X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)
```

----

### Training Data

* Training data is now in the X_train and Y_train arrays for preparing models
* We will be using X_validation and Y_validation sets later

----

### Test Harness

* We will use 10-fold cross validation to estimate accuracy
* This splits our dataset into 10 parts
* We train on 9 parts, test on 1
* Repeat for all combinations of train-test splits.

----

### Test options and evaluation metric

```python
# Test options and evaluation metric
seed = 7
scoring = 'accuracy'
```

----

### We use 'accuracy' to evaluate models

* The ratio of number of correctly predicted instances divided by total number of instances
* This will be our scoring variable when we run, build, and evaluate models

----

### Build Models

* Initially, we don't know which algorithms/configurations to use
* We know from plots that some classes may be linearly separable
* Therefore, we're expecting some good results

----

### Evaluate Algorithms

* Logistic Regression (LR)
* Linear Discriminant Analysis (LDA)
* K-Nearest Neighbors (KNN)
* Classification and Regression Trees (CART)
* Gaussian Naive Bayes (NB)
* Support Vector Machines (SVM)

----

### Algorithm Choices and Execution

* This is a good mixture of simple linear (LR and LDA), and nonlinear (KNN, CART, NB and SVM) algorithms
* We will reset the random number seed before each run
* This ensures that the evaluation of each algorithm is performed using exactly the same data splits
* Makes sure the results are directly comparable
----

### Build and Evaluate

```python
# Spot Check Algorithms
models = []
models.append(('LR', LogisticRegression(solver='liblinear', multi_class='ovr')))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC(gamma='auto')))
# evaluate each model in turn
results = []
names = []
for name, model in models:
	kfold = model_selection.KFold(n_splits=10, random_state=seed)
	cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
	results.append(cv_results)
	names.append(name)
	msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
	print(msg)
```

----

### Select Best Model

* We now have 6 models and accuracy estimations for each
* We need to compare the models to each other and select the most accurate

----

### Model Results

```python
LR: 0.966667 (0.040825)
LDA: 0.975000 (0.038188)
KNN: 0.983333 (0.033333)
CART: 0.975000 (0.038188)
NB: 0.975000 (0.053359)
SVM: 0.991667 (0.025000)
```

----

### Create a plot of the model evaluation results

```python
# Compare Algorithms
fig = plt.figure()
fig.suptitle('Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels(names)
plt.show()
# the following is just for our online python interpreter
plt.savefig('graph.png')
```

---

### Make Predictions

----

### Let's use the KNN algorithm

* Simple algorithm
* Fairly accurate in our example
* Now we want to get an idea of the accuracy of the model on our validation set
* Will give us an independent final check on accuracy of best model

----

### Why was keeping a validation set useful?

* In case we made a slip during training
* Examples would be overfitting to our training set, or data leak
* Both of those instances would result in an overly optimistic result

----

### Run KNN directly on validation set

* Final accuracy score
* Confusion Matrix
* Classification report

----

### Make predictions on validation dataset
	
```python
# Make predictions on validation dataset
knn = KNeighborsClassifier()
knn.fit(X_train, Y_train)
predictions = knn.predict(X_validation)
print(accuracy_score(Y_validation, predictions))
print(confusion_matrix(Y_validation, predictions))
print(classification_report(Y_validation, predictions))
```

----

### Results

```python
0.9

[[ 7  0  0]

[ 0 11  1]

[ 0  2  9]]

                 precision    recall  f1-score   support

    Iris-setosa       1.00      1.00      1.00         7

Iris-versicolor       0.85      0.92      0.88        12

Iris-virginica       0.90      0.82      0.86        11

      micro avg       0.90      0.90      0.90        30

      macro avg       0.92      0.91      0.91        30

   weighted avg       0.90      0.90      0.90        30
```

---

### Hooray, we've done our first machine learning project!

----

### What did we learn?

* You don't need to understand everything right away
* You don't need to know how the algorithms work now - that can come later
* You don't need to be a Machine Learning expert to start tinkering!

----

### What are the key steps of a Machine Learning Project?

* Loading Data
* Looking at Data
* Evaluating Algorithms
* Making Predictions

----

## Thanks for coming!!



    