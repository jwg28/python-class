# Load libraries
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = pd.read_csv('iris.csv', names=names)

dataset['class'] = dataset['class'].replace(to_replace={'.*IVC.*': 'Iris-versicolor'}, regex=True)

group_by_class = dataset.groupby(by=['class'])
class_avg = group_by_class.mean()
class_data_count = group_by_class.count()

"""
Since all the columns in car_data_count are the same, we will use just the first column as the rest 
yield the same result. iloc allows us to take all the rows and the zeroth column.
"""
dataset_count_series = class_data_count.iloc[:,0]

features_of_interest = pd.DataFrame({'sepal_length': class_avg['sepal-length'], 'sepal_length_count': dataset_count_series})

print(features_of_interest)

alternative_method = class_avg.merge(class_data_count, left_index=True, right_index=True, suffixes=['_avg','_count'])

alternative_method[['sepal_length_avg', 'sepal_length_count']].sort_values(by=['sepal_length_avg', 'sepal_length_count'], ascending=True).plot(kind='barh')
