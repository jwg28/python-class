# How to program?

## Using Python :)

---

## Why are we here?

### Learn basic concepts of programming

----

## What I wish I was taught:

----

## Programming isn't about languages

----

### 'What language should I learn?' is the WRONG question to ask
* The language you choose ultimately doesn't matter that much
* The difficulty in learning programming isn't the languages, but PARADIGMS

----

###  Contrary to popular belief, programming doesn't require a lot of memorizing

* You wouldn't memorize a dictionary, or Wikipedia right?
* Rather, you'll be learning that these concepts exist, and then utilize these manuals as references

----

### Most programming isn't about Math!

* It's mostly about logic
* The math you'll need is the math required for the problem you're trying to solve anyway

----

### Programming languages are simpler than human languages

* How many defined words are there in the English language?
* How about Python?

----

### They have painfully consistent syntax

* Programming languages don't have much, if any room, for flexibility/interpretation
* Think of the phrase 'Tell me about it!'

----

### At its core, programming is all about solving problems

----

### Coding has ~8 main concepts

* They work in nearly the same way in every programming language
* What we want to do is learn how to use these concepts in English
    * The secret, again, isn't CODE - it's ALGORITHMS/PARADIGMS

----

###  Last but not least - Comments are Code!

* One of the great misconceptions is that comments explain code to other programmers
* It's actually the other way around - Code is there to explain comments to the computer

----
  
### Now let's get to some of the ~8 main concepts!

---

### (1) New variable

* Name: what do we call this thing?
* Type: what type of data does it contain?
* InitVal: what is its starting value?

----

### New variable Algorithm

* Create a variable called (Name) of type (Type) with the value (InitVal)

```python
name = initVal
```

----

### (2) Output

* Message: text to write to user

----

### Output algorithm

* Output the text (Message)

```python
print ('message')
```

----

### (3) Input

* Variable: where answer from the user will be stored
* Message: question being asked of the user

----

### Input algorithm

* Ask the user (Message) and store the answer in (Variable)

```python
variable = input('message')
```

----

### With these first 3 concepts, we can write a program!

* Ask the user for 2 numbers, then add them
* On paper (or text editor)
* NO CODE!  Algorithm only

----

### My first try:

* create an integer variable for x
* create an integer variable for y
* create an integer variable for sum
* ask the user 'X:' and put the answer in x
* ask the user 'Y:' and put the answer in y
* put x + y in sum
* tell user 'Sum is ' sum

----

### Then, convert to comments

----

### Flesh out the comments

```python
# create an integer variable for x
x = 0
# create an integer variable for y
y = 0
# create an integer variable for sum
sum = 0

# ask the user 'X:' and put the answer in x
x = input('X: ')
# ask the user 'Y:' and put the answer in y
y = input('Y: ')

# put x + y in sum
sum = x + y
# tell user 'Sum is ' sum 
print ('Sum is: ')
print(sum)
```

----

### Uh oh.

----

### It's ok - failure is wonderful!

----

### How to debug

* The best way to debug is to not have bugs! :D
* Bad implementation can always be googled
* Bad algorithms/bad understanding of the problem space can't be googled
* There's probably something you're not understanding
* Something that you're trying to do isn't doing what you think it's supposed to do
* Start by truly understanding the problem
  
----

### What happened in our example?

* Easy to assume that the + sign is broken
* That's not really the problem though
* Try print("python" + "meetup")

----

### Now, we can add a new tool to our toolbox!

----

### (4) Convert to Integer

* oldVariable: in a non-integer format
* intVariable: integer to store results

----

### Convert to Integer Algorithm (English)

* Convert (oldVariable) to integer and store in (intVariable)

```python
intVariable = int(oldVariable)
```

----

### Try again with new tool:
```python
# create an integer variable for x
# create an integer variable for y
# create an integer variable for sum
# ask the user 'X:' and put the answer in x
# ask the user 'Y:' and put the answer in y
# convert x to integer
# convert y to integer
# put x + y in sum
# tell user 'Sum is ' sum
```

----

### Version 2.0:

```python
# create an integer variable for x
x = 0
# create an integer variable for y
y = 0
# create an integer variable for sum
sum = 0

# ask the user 'X:' and put the answer in x
x = input('X: ')
# ask the user 'Y:' and put the answer in y
y = input('Y: ')

# convert x to integer
x = int(x)
# convert y to integer
y = int(y)

# put x + y in sum
sum = x + y
# tell user 'Sum is ' sum 
print ('Sum is: ')
print(sum)
```

----

### SUCCESS!!!

----

### (5) For Loop

* sentry: integer variable that will control loop
* start: integer value of sentry at beginning
* finish: integer value of sentry at end
* change: integer to add to sentry at each pass

----

### For Loop Algorithm

* Begin with (sentry) at (start) and add (change) to sentry on each pass until sentry is larger than or equal to (finish)

```python
for sentry in range(start, finish, change):
    <code>
```

----

### (6) While Loop

* sentry: variable that will control loop
* initializationCode: code that initializes sentry
* condition: loop repeats if condition is true
* changeCode: code to change sentry so condition is triggered

----

### While Loop Algorithm

* Initialize (sentry) with (initializationCode) then continue loop as long as (condition) is true.  Inside loop, change sentry with (changeCode)

```python
initializationCode
while (condition):
    changeCode
```

----

### Multiple Exits

* Consider a basic password loop
* Exits with a positive result if the user enters the right password
* Exits with a negative result if the user is wrong 3 times
* How would you code this loop?

----

### Let's muddle through our algorithm!

----

### The keepGoing loop:
    
```python
correct = "colabPython"
tries = 0

keepGoing = True
while(keepGoing):
    tries = tries + 1
    print ("try # : ", tries)

    guess = input("What's the password? ")
    if guess == correct:
        print("That's correct! Here's the treasure")
        keepGoing = False
    
    elif tries >= 3:
        print("Too many wrong tries. Firing the missiles.")
        keepGoing = False
```

----

### Other concepts to teach later:

* Lists & tuples
* String manipulation
* Working with built-in help
* List iterations
* Functions (and scope, and parameters, and return)
* Objects

---

### What did we learn today?

* Programming isn't about languages
* Programming doesn't require a lot of memorizing
* Most programming isn't about Math!
* Programming languages are simpler than human languages
* At its core, programming is all about solving problems
* Coding has ~8 main concepts
* Last but not least - Comments are Code!

----

### Main Concepts

* New variable
* Output
* Input
* Convert types
* For Loop
* While Loop
* ...and more!

----

## Thank You!

If you're interested in more classes, visit colab.duke.edu/roots
    