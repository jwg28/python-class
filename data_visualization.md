# Intro to Data Visualization

### With Python :)

----

## Why are we here?

* Get a basic introduction to Machine Data Visualization
* Perform some basic Data Visualizations

----

## What will we be doing today?

* Reading Data From CSV
* Formatting, cleaning and filtering Data Frames
* Group-by
* The Plot Function Basics

---

## Basic Requirements

* Reading Data from CSV
* Formatting, cleaning and filtering Data Frames
* Group-by

----

## Reading CSV / Required Imports for Matplotlib & Pandas

```python
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import matplotlib as mpl
import os

# The following section is just for our online class
mpl.use('Agg')
if os.path.exists('graph.png'):
  os.remove('graph.png')

names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = pd.read_csv('iris.csv', names=names)
```

----

### Notes on previous code

* read_csv returns a DataFrame
* the file path is relative to that of your notebook.

----

## Formatting, Cleaning and Filtering DataFrames

* We can use the `columns` property to see names of columns
* `head(nRows)` function allows us to see some number of rows
* We can view types of values for categories

----

### View columns and sample rows

```python
print(dataset.columns)
print(dataset.head(2))
```

----

### View category values

```python
print(dataset['class'].unique())
```

----

### Removing Empty Data

```python
dataset = dataset.dropna(subset=['class'])
```

----

### Standardizing Data

```python
dataset['class'] = dataset['class'].replace(to_replace={'.*IVC.*': 'Iris-versicolor'}, regex=True)
```

----

### Filter out unwanted values

```python
dataset = dataset[(dataset['class'] != 'undefined')]
```

----

### Review category values

```python
print(dataset.columns)
print(dataset.head(2))
```

----

### Notes

* pandas never mutates existing data
* We therefore have to overwrite data manually

----

## Group-by

* Group-by’s can be used to build groups of rows based off a specific feature
* We can then perform operations (min, max, std) on these groups
* This helps us describe the sample data

----

## Group-by

```python
group_by_class = dataset.groupby(by=['class'])
class_avg = group_by_class.mean()
print(class_avg)
class_data_count = group_by_class.count()
print(class_data_count)
```

---

## Visualizing your Data

* The pandas `plot` function

----

### Two types of plots:

* Univariate plots to better understand each attribute.
* Multivariate plots to better understand the relationships between attributes.

----

### Univariate Plots

* Plots of each individual variable
* Since input variables are numeric, we can create box and whisker plots of each

----

### Pandas Plot Function Parameters

* kind
* color
* linestyle
* xlim, ylim
* legend
* labels
* tilte

----

## Pandas Plot Types


* `bar` or `barh` for bar plots
* `hist` for histogram
* `box` for boxplot
* `kde` or `density` for density plots
* `area` for area plots
* `scatter` for scatter plots
* `hexbin` for hexagonal bin plots
* `pie` for pie plots

----

### Box and Whisker Plots

```python
# box and whisker plots
dataset.plot(kind='box', subplots=True, layout=(2,2), sharex=False, sharey=False)
plt.show()
# the following is just for our online python interpreter
plt.savefig('graph.png')
```

----

### Histograms are a way for us to get an idea of data distribution

----

### Histograms

```python
# histograms
dataset.hist()
plt.show()
# the following is just for our online python interpreter
plt.savefig('graph.png')
```

----

### Multivariate Plots

* Now we can look at the interactions between the variables
* First, let’s look at scatterplots of all pairs of attributes
* Scatterplots help us spot structured relationships between input variables

----

### Scatter Plot

```python
from pandas.plotting import scatter_matrix

# scatter plot matrix
scatter_matrix(dataset)
plt.show()
# the following is just for our online python interpreter
plt.savefig('graph.png')
```

----

### Observations

* Note the diagonal grouping of some pairs of attributes
* This suggests a high correlation and a predictable relationship

----

### Let's play around with different plot types!

---

### Hooray!  We've visualized some data!

----

### What did we learn?

* Reading Data From CSV
* Formatting, cleaning and filtering Data Frames
* Group-by
* The Plot Function Basics

----

## Thanks for coming!!



    