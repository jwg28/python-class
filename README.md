# README.md - jeff-classes

This repository contains slides for most of the classes I taught in the 2019 Fall Semester.

The file names should be self-explanatory - one note is that the class_links.html file has a collection of links to the repl's that I used for the introduction to JS class (which is actually a Google Slide, and not in this repository)

You will need reveal-md installed on your machine in order to run the slideshows.

To view a sample slideshow:
reveal-md --theme=white --css=reveal.css beginning_python.md
